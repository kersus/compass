package com.sogou.bizdev.compass.sample.mybatis.shard.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sogou.bizdev.compass.sample.common.po.Plan;
import com.sogou.bizdev.compass.sample.common.po.PlanExample;

public interface ShardMybatisPlanDao {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    int countByExample(PlanExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    int deleteByExample(PlanExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    int deleteByPrimaryKey(Long cpcplanid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    int insert(Plan record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    int insertSelective(Plan record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    List<Plan> selectByExample(PlanExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    Plan selectByPrimaryKey(Long cpcplanid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    int updateByExampleSelective(@Param("record") Plan record, @Param("example") PlanExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    int updateByExample(@Param("record") Plan record, @Param("example") PlanExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    int updateByPrimaryKeySelective(Plan record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cpcplan_0101
     *
     * @mbggenerated Mon May 12 17:27:03 GMT+08:00 2014
     */
    int updateByPrimaryKey(Plan record);
}